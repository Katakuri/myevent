import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule} from '@angular/material/slider';

import {ReactiveFormsModule } from '@angular/forms';

import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {MatCheckboxModule} from '@angular/material';
import {MatButtonModule} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatMenuModule} from '@angular/material/menu';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatStepperModule} from '@angular/material/stepper';
import {MatTabsModule} from '@angular/material/tabs';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import { LoginComponent } from './login/login.component';
import {FormsModule} from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { Routes , RouterModule} from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { CitiesService } from './services/cities.service';
import { CreateEventComponent } from './events/create-event/create-event.component';
import { ListEventsComponent } from './events/list-events/list-events.component';
import { EventService } from './services/event.service';
import { AuthInterceptor } from './services/interceptor.service';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { AuthGuardService } from './services/auth-guard.service';
import { WeaherService } from './services/weaher.service';
import { EventDetailsComponent } from './events/event-details/event-details.component';
import { AboutUserComponent } from './about-user/about-user.component';
import { ConfirmDeletedUserComponent } from './about-user/confirm-deleted-user/confirm-deleted-user.component';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent},
  { path: 'eventCreate', canActivate: [AuthGuardService], component: CreateEventComponent },
  { path: 'eventList', canActivate: [AuthGuardService], component: ListEventsComponent },
  { path: 'not-found', component: FourOhFourComponent },
  { path: 'aboutUser', canActivate: [AuthGuardService], component: AboutUserComponent },
  { path: '**', redirectTo: 'not-found' }
];


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    RegisterComponent,
    CreateEventComponent,
    ListEventsComponent,
    FourOhFourComponent,
    EventDetailsComponent,
    AboutUserComponent,
    ConfirmDeletedUserComponent
    ],
  imports: [
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatCheckboxModule,
    MatButtonModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    HttpClientModule,
    MatNativeDateModule,
    MatRippleModule
  ],
  entryComponents: [EventDetailsComponent, ConfirmDeletedUserComponent],
  providers: [AuthGuardService, CitiesService, AuthInterceptor, EventService,
     { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }, WeaherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
