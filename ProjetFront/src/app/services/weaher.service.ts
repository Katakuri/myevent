import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class WeaherService {

  constructor(
    public http: HttpClient
  ) { }

  getWeathereCity(id){
    return this.http.get<any>('http://localhost:4000/save/weatherCity/' + id);
  }

  unixToDate(unix){
    const day = moment.unix(unix);
    return day;
  }

  kelvinToCelsius(kelvin){
    return kelvin - 273;
  }
}
