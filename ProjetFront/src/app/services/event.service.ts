import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  events = [];

  constructor(
    public http: HttpClient
  ) { }

  getAllEvents() {
    const promise = new Promise((resolve, reject) => {
      this.http.get<any>('http://localhost:4000/event/allEvents')
      .toPromise()
      .then(
        res => {
          console.log(res);
          this.events = res;
          resolve();
        },
        msg => {
          reject();
        },
        );
    });
    return promise;
  }

  getEventsCompleted() {
    const promise = new Promise((resolve, reject) => {
      this.http.get<any>('http://localhost:4000/event/getEventCompleted')
      .toPromise()
      .then(
        res => {
          console.log(res);
          this.events = res;
          resolve();
        },
        msg => {
          reject();
        },
        );
    });
    return promise;
  }

  getEventsStill() {
    const promise = new Promise((resolve, reject) => {
      this.http.get<any>('http://localhost:4000/event/getEventsDontStart')
      .toPromise()
      .then(
        res => {
          console.log(res);
          this.events = res;
          resolve();
        },
        msg => {
          reject();
        },
        );
    });
    return promise;
  }

  getEventsByUser(){
    return this.http.get<any>('http://localhost:4000/api/getEventsByUser/');
  }

  participateToEvent(event) {
    return this.http.post('http://localhost:4000/event/participatEvent/' + event._id, event);
  }

  deleteUser(){
    return this.http.delete('http://localhost:4000/api/deleteUser/');
  }
}
