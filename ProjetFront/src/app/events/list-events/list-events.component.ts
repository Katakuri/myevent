import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/services/event.service';
import {environment} from '../../../environments/environment';
import * as mapboxgl from 'mapbox-gl';
import { CitiesService } from 'src/app/services/cities.service';
import { WeaherService } from '../../services/weaher.service';
import {MatDialog} from '@angular/material/dialog';
import {EventDetailsComponent} from '../event-details/event-details.component';
import * as moment from 'moment';


@Component({
  selector: 'app-list-events',
  templateUrl: './list-events.component.html',
  styleUrls: ['./list-events.component.css']
})
export class ListEventsComponent implements OnInit {
  map: mapboxgl.Map;
  style = 'mapbox://styles/mapbox/streets-v11';
  lat = 37.75;
  lng = -122.41;
  currentCity: any;

  allEvents = [];

  constructor(
    private eventService: EventService,
    private cityService: CitiesService,
    public dialog: MatDialog

    ) { }

  ngOnInit() {
    this.getAllEvents();

    (mapboxgl as typeof mapboxgl).accessToken = environment.mapbox.accessToken;
    this.map = new mapboxgl.Map({
        container: 'map',
        style: this.style,
        zoom: 12,
        center: [2.21667, 48.933331]
    });
    // Add map controls
    this.map.addControl(new mapboxgl.NavigationControl());
  }

  getAllEvents() {
    this.eventService.getAllEvents().then(() => {
      this.allEvents = this.eventService.events;
    });
  }
  getEventCompleted() {
    this.eventService.getEventsCompleted().then(() => {
      this.allEvents = this.eventService.events;

    });
  }

  getEventStill() {
    this.eventService.getEventsStill().then(() => {
      this.allEvents = this.eventService.events;
    });

  }
  openDialog(element) {
    const dialogRef = this.dialog.open(EventDetailsComponent, {
      data : element
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.ngOnInit();
      }
    });
  }

  checkDate(dateEvent){
    var toDay = moment();
    var eventDate = moment(dateEvent);
    if (toDay > eventDate ){
      return true;
    }
    return false;
  }

  flyTo(city: string) {
    this.cityService.getCityByName(city).subscribe((res) => {
      console.log(res);
      this.currentCity = res[0];
    },
      (err) => { console.log(err);
      },
      () => {
        console.log(this.currentCity);
        this.map.flyTo({
          center: [this.currentCity.coord.lon, this.currentCity.coord.lat]
        });
      });
  }
}
