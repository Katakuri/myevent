var schedule = require('node-schedule');
const mongoose = require('mongoose');
const Event = require('../models/event');
const config = require('../tools/config');
const emailModule = require('../tools/email');

 
var j = schedule.scheduleJob('* * * * *', function(){
    var mailOpt = emailModule.mailOptions;
    var sendEmail = emailModule.sendEmail;

        // Préparation de l'email
        console.log(mailOpt.to);
    Event.find({dateEndEvent : {$gte : new Date()}}).then( (events) => {
        if(events){
          events.forEach((event) => {
            event.idUsersParticipate.forEach((user) => {
                console.log(event.title);
                console.log(user);
                console.log(event.dateStartEvent);
                mailOpt.subject = 'Rappel pour votre evenement '+event.title;
                mailOpt.to = 'myevent.miage@gmail.com,'+user;
                mailOpt.text = "N'oubliez pas ! Vous participez à l'evenement "+event.title+ " qui aura lieu le "+ event.dateStartEvent;
                sendEmail(mailOpt);
            });
          })
      }else {
          console.log("Pas d'evenement aujd")
      }
  });
});

module.exports = j;