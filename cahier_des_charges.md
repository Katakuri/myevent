## *User Story Participant*

#### *Description* : 

En tant qu’utilisateur je souhaite créer un évènement afin de pouvoir l’organiser. Pour le créer, je passe par la plateforme MYEVENT. 
Je dois alors m’inscrire sur cette application afin d’avoir la possibilité de créer et partager l'évènement que je souhaite organiser. 
Lorsque je m’inscris, je choisis l’option d’inscription proposée. 
Je vais ensuite faire les démarches nécessaires afin que mon compte soit finalisé. Après cette étape, je me connecte pour pouvoir entrer dans l’application. 
J’ai maintenant la possibilité de créer mon évènement. Je clique donc sur “créer un évènement”. 
Après avoir renseigné les informations propres à mon évènement, ce dernier est mis en ligne. 
A chaque fois que quelqu’un rejoint mon évènement, je reçois une alerte. Après que mon évènement ait été organisé, celui-ci reste sur la plateforme (est supprimé). 


### *Règles métier*

- Le nombre de caractère sera limité à 25 pour le nom et prénom.
- Une personne ne peut pas insérer de chiffre dans son prénom ou nom.
- Une personne ne peut pas avoir deux comptes avec le même mail.
- Le mot de passe doit contenir 8 caractères.
- Tous les champs d’inscription sont obligatoires (Nom d’utilisateur, mot de passe, la ville et l'age).
- Pour la date de l'évènement, la date de fin ne doit pas être avant la date de début.
- Le nombre de participants est choisi par l'organisateur

### *Test des règles métier A FAIRE*

Il faudra tester : 

- L'un des champs est vide
- Une même adresse mail insérée une deuxième fois  
- Le mot de passe ne respecte pas la convention 



## Inscription

Si un utilisateur souhaite s'inscrire sur la plateforme afin de créer ou participer à un évènement il devra renseigner différentes informations. Dans un premier temps, son adresse mail ainsi que son mot de passe pour la connexion. De plus, il faudra son nom, prénom, date de naissance. Après son inscription, l'utilisateur reçoit un mail de confirmation de la part de "myevent.miage@gmail.com".

*Règles métier* :

- Le nombre de caractère sera limité à 30 pour le nom et prénom.
- Une personne ne peut pas insérer de chiffre dans son prénom ou nom.  
- Une personne ne peut pas avoir deux comptes avec le même mail.
- Le mot de passe et la vérification mot de passe doit-être identique. ***
- Tous les champs d’inscription sont obligatoires (Sexe, nom d’utilisateur, mot de passe, vérification du mot de passe, la ville et age).
- Les informations sur le compte de l'utilisateur avec une liste de ses événements organisés sera disponible dans "Information du


## Connexion

Pour accéder aux services de l'application, les utilisateurs (qu'ils soient organisateurs ou participants) doivent se connecter. 
Si un utilisateur externe souhaite participer à un évènement, il devra se connecter. 
Il faudra s'inscrire sur la plateforme si l'utilisateur se connecte pour la 1ere fois. Une fois connecté, l'utilisateur peut créer pour participer à un évènement.

*Règles métier* :

- L'adresse e-mail doit correspondre à une adresse correcte (avec un identifiant, un @, et un "." ).
- Le mot de passe doit contenir 8 caractères.


## Création de l'évènement 

Pour créer un évènement, l'organisateur se connecte. Ensuite, il ira dans l'espace de création d'évènement. Pour créer son évènement, plusieurs informations sont nécessaires telles que le titre,la description, la date de début et de fin, l'âge minimum, la catégorie, et le nombre de personnes.
Une fois ces informations inscrites et enregistrées, l'évènement est accessible publiquement dans la liste des évènements.

*Règles métier* :

- Le nom de l'évènement ne peut dépasser 50 caractères
- Le description de l'évènement ne peut depasser 1000 caractères.
- Pour la date de l'évènement, la date de fin ne doit pas être avant la date de début. ***
- Le nombre de participants choisis ne peut être inférieur à 1. ***
- Le nombre de participants est choisi par l'organisateur.
- La ville doit être séléctionnée dans les choix.
- Aucun des champs ne peut être null.

*Catégories des évènements créés* : 

Informatique / Sport / Cuisine / Lecture-Langue-Culture / Photographie / Musique / Bien-Etre (mood) / Film / Mode-Beauté brush/ Art


## Trouver un évènement 

Pour trouver un évènement, l'utilisateur peut trouver tous les évènements dans l'espace "Liste des evenements".
Ensuite, il peut choisir celui qui l'intéresse. 

*Règles métier* :

- La carte localise les événements séléctionnés.
- Les évènements à venir auront une pastille verte
- Tous les évènements sont présents même ceux qui sont complets.
- Un évènement terminé aura une pastille rouge indicative.
- On peut séléctionner les événements finis et les événements qui approchent. 
- On peut trier les événements en fonction de leurs catégories.

## Rappel de participation

Chaque participants à un événement, recevra un rappel par e-mail le matin de l'événement à 9h. Les participants se souviendront donc qu'ils ont un événement dans la journée.

## Météo

Pour chaque événement séléctionné par l'utilisateur, celui-ci pourra voir la météo à l'heure actuelle. Cela lui permettra de savoir s'il fait beau en fonction de l'événement qu'il veut faire. Cette fonctionnalité est très utile pour les événements en plein air. 

*Règles métier* :

- Pour chaque type de temps, une icône correspondra.
- Une description sera visible.
- Les températures seront indiquées.
- Les heures de levé et couché de soleil seront affichées.

## Participation

Pour participer à un évènement, l'utilisateur va sur l'évènement auquel il souhaite participer. Arrivé sur la page de l'évènement, ce dernier appuie sur "Participer". S'il est connécté sa participation est automatiquement enregistrée mais s'il ne l'est pas, il ne pourra pas s'y inscrire. L'utilisateur a donc l'obligation de se connecter avant de s'inscrire.
Le nombre de places pour participer à l'évènement va donc diminuer de -1. Après sa demande de participation, l'utilisateur reçoit un mail de confirmation de la part de "myevent.miage@gmail.com".

*Règle métier* :

- Le nombre de participants restant diminue de -1.
- L’utilisateur ne peut pas participer à un évènement complet.
- Un message "évènement complet" est affiché lorsqu'il ne reste plus de places. La participation n'est plus possible.
- L'utilisateur doit être connecté ou inscrit si ce dernier veut participer à un évènement.
- L'utilisateur ne peut pas participer à un évènement terminé. ***
- Le participant ne peut pas avoir un age inférieur à l'âge minimum. ***
- Si le participant à l'âge minimum, il peut participer à l'évènement.


## Deconnection
Pour se deconnecter de myEvent, l'utilisateur va devoir cliquer sur les 3 points en haut à droite, puis arriver dessus, il devra cliquer sur "Logout".

*Règle métier* :

- L'utilisateur non connecté ne peut pas créer un événement.
- L'utilisateur non connecté ne peut pas accéder à la liste d'événements.
- Si l'utilisateur souhaite créer un événement ou aller dans la liste d'événements, il devra se connecter. 


## Suppresion de compte
Pour pouvoir supprimer son compte de myEvent, l'utilisateur va devoir cliquer sur "Information du compte", puis arrivé dessus, il devra cliquer sur "Supprimer mon compte" et valider l'action sur le pop-up qui apparaitra.

*Règle métier* :

- Le profil de l'utilisateur ne peut pas être retrouvé. ***
- L'utilisateur ne peut plus se connecter. ***
- Si l'utilisateur souhaite revenir sur le site pour participer à un évènement, il devra s'inscrire et créer un nouveau compte (il pourra utiliser la même adresse mail que son compté supprimé). 