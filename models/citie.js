const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CitieSchema = new Schema ({
    id: {
        type : Number
    },
    name : {
        type : String,
        index: { unique: true }
    },
    country : {
        type : String
    }, 
    coord : {
        lon : {
            type : Number
        },
        lat : {
            type : Number
        }
    }
});

const Citie = mongoose.model('citie', CitieSchema);
module.exports = Citie;